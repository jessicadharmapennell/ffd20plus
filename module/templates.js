/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {
  const GAMEID="ffd20plus";

  // Define template paths to load
  const templatePaths = [
    // Attribute list partial.
    `systems/${GAMEID}/templates/parts/sheet-attributes.html`,
    `systems/${GAMEID}/templates/parts/sheet-groups.html`
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
