/**
 * Define your class that extends FormApplication
 */
export class MyFormApplication extends FormApplication {
  constructor(exampleOption) {
    super();
    this.exampleOption = exampleOption;
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['form'],
      popOut: true,
      template: `systems/ffd20plus/templates/myFormApplication.html`,
      id: 'my-form-application',
      title: 'My FormApplication',
      height: '1200px',
      width: '950px',
    });
  }

  getData() {
    // Send data to the template
    return {
      msg: this.exampleOption,
      color: 'red',
    };
  }

  activateListeners(html) {
    super.activateListeners(html);
  }

  async _updateObject(event, formData) {
    console.log(formData.exampleInput);
  }
}

window.MyFormApplication = MyFormApplication;